document.getElementById("submit").addEventListener("click", addListItem);
document.getElementsByTagName("body")[0].addEventListener("keypress", (e) => {
    if (e.key === "Enter") {
        addListItem();
    }
});
document.getElementById("search").addEventListener("keyup", searchList);
list = document.getElementById("items");

function addListItem() {
    let itemName = document.getElementById("item-name").value;
    if (itemName.length !== 0) {
        if (checkForNothingAddedElement(list.children)) {
            list.children[0].remove();
        }
        let listElement = document.createElement("li");
        let spanElement = document.createElement("span")
        let button = document.createElement("button");

        spanElement.innerText = itemName;
        button.innerText = "x";

        button.addEventListener("click", deleteElement);

        listElement.appendChild(spanElement);
        listElement.appendChild(button);
        list.appendChild(listElement);
        document.getElementById("item-name").value = "";
    }
}

function deleteElement(e) {
    e.target.parentElement.remove();
    let lists = document.getElementById("items");
    if (lists.childElementCount === 0) {
        let h3 = document.createElement("h3");
        let li = document.createElement("li");
        h3.innerText = "nothing added";
        li.appendChild(h3);
        lists.appendChild(li)
    }
}

function searchList(e) {
    let list = document.getElementById("items").children;
    let searchQuery = document.getElementById("search").value.toLowerCase();
    console.log(searchQuery);
    if (!checkForNothingAddedElement(list)) {
        Array.from(list).forEach((item) => {
            let itemName = item.children[0].innerText;
            if (itemName.toLowerCase().indexOf(searchQuery) !== -1) {
                item.style.display = "flex"
            } else {
                item.style.display = "none"
            }
        });
    }
}

function checkForNothingAddedElement(childList) {
    return childList[0].children[0].tagName === "H3" ? true : false;
}